const server = require('../wikipedia_graph');
const supertest = require('supertest');
const requestWithSupertest = supertest(server);

describe("User Endpoints", () => {
  it(`Check connect to /fetch`, async () => {
    const res = await requestWithSupertest.get('/fetch');
    expect(res.status).toEqual(200);
    expect(res.text).toEqual(`<img src="/mychart_1.png" style="max-width:100vw;max-height:100vh;padding:1rem;box-sizing:border-box;">`);
  });
});
