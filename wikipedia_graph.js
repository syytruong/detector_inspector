// Import required packages
const express = require("express");
const cheerio = require("cheerio");
const request = require("request");
const app = express();
const ChartJsImage = require("chartjs-to-image");

app.use(express.static('docs'))

// Route to fetch data from the Wikipedia page
app.get("/fetch", (req, res) => {
  // URL of the Wikipedia page
  const url =
    "https://en.wikipedia.org/wiki/Women%27s_high_jump_world_record_progression";

  // Fetch the data from the URL
  request(url, (error, response, html) => {
    if (!error && response.statusCode == 200) {
      // Load the fetched data into cheerio
      const $ = cheerio.load(html);

      $("table.wikitable").each(async (index, el) => {
        let headers = {};
        let list = [];

        // Get content each body row to get th tags and header's content and td tags for values
        $(el)
          .find($("tbody tr"))
          .each((index, elTr) => {
            if ($(elTr).find($("th")).length > 0) {
              $(elTr)
                .find($("th"))
                .each((index, elTh) => {
                  headers[replaceSpace($(elTh).text().trim().toLowerCase())] =
                    $(elTh).text().trim();
                });
            }

            if ($(elTr).find($("td")).length > 0) {
              let data = {};
              $(elTr)
                .find($("td"))
                .each((index, elTd) => {
                  const match = $(elTd)
                    .text()
                    .trim()
                    .match(/(^\d.?\d+)/g);
                  data[Object.keys(headers)[index]] =
                    match?.length > 0 ? +match[0] : $(elTd).text().trim();
                });

              list.push(data);
            }
          });

        let labels = [];
        let label = "";
        let numericValues = [];

        // Check through every field
        // If field's value is numeric then set it for label and push to numericArrays
        // Else push it to labels
        for (const field in headers) {
          if (list.every((i) => typeof i[field] === "number")) {
            label = field;
            numericValues = list.map((i) => i[field]);
          }
          if (
            list.every((i) => typeof i[field] === "string") &&
            !labels.length
          ) {
            labels = list.map((i) => i[field]);
          }
        }

        // // Generate the chart
        const chart = new ChartJsImage();
        chart.setConfig({
          type: "bar",
          data: {
            labels,
            datasets: [{ label, data: numericValues }],
          },
        });
        chart
          .setWidth(numericValues.length * 30)
          .setHeight(numericValues.length * 30 * 0.4);
        await chart.toFile(__dirname + `/docs/mychart_${index + 1}.png`);

        res.send(`<img src="/mychart_${index + 1}.png" style="max-width:100vw;max-height:100vh;padding:1rem;box-sizing:border-box;">`)
      });
    }
  });
});

// Start the server
const port = process.env.PORT || 5005;
app.listen(port, () => {
  console.log(`Server started on port ${port}`);
});

function replaceSpace(str) {
  return str.replace(/\s/g, "_");
}

module.exports = server;