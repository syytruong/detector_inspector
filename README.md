# Detector Inspector Software Engineer Challenge

Thank you for taking the time to complete this challenge. It helps us understand your experience and the way you approach and solve problems.

Please read the [CHALLENGE](CHALLENGE.md) document for the details of the challenge task.

## Getting started

We would recommend that you clone or download this repository to your local computer. Please don't fork the repository as others will be able to see your answers.

You can then create your own repository and copy this repository to that (exclude the `.git` folder).

## Submitting

If you have created your own repository please reply to the email you received with a link to your repository.

You can also send us back a `zip` file with your answers.

## How to run

Run npm install to install the packages
Run npm run dev to run the project
Run npm run test to run test

## Strategy

Using "request" to fetch a Wikipedia page then extracts data from HTML tables with "cheerio". Then use "chartjs-to-image" to create a graph based on data extracted from the tables